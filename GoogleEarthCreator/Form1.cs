﻿using System;
using System.Configuration;
using System.IO;
using System.ComponentModel;
using System.Windows.Forms;

namespace GoogleEarthCreator
{
    public partial class Form1 : Form
    {
        ParseCSV csvParser = new ParseCSV();

        public Form1()
        {
            InitializeComponent();
            exportBackgroundWorker.WorkerReportsProgress = true;
            parseBackgroundWorker.WorkerReportsProgress = true;

            // Initialize the dataset stuff
            csvDataGridView.AutoGenerateColumns = true;
            csvDataGridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);
            var connectionString = ConfigurationManager.ConnectionStrings["GoogleEarthCreator.Properties.Settings.uploadsConnectionString"];//.ConnectionString;
            tableAdapterManager.uploadsTableAdapter.Connection.ConnectionString = connectionString.ConnectionString;
            tableAdapterManager.uploadsTableAdapter.Fill(uploadsDataSet.uploadsTable);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // A more robust way of exiting, but will most likely only ever use Application.Exit...
            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }
        }

        private void csvDataGridView_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (csvDataGridView.Columns[e.ColumnIndex].HeaderText.Contains("Icon"))
            {
                try
                {
                    System.Diagnostics.Process.Start(csvDataGridView.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
                }
                catch (Win32Exception)
                {
                    string message = "This file doesn't exist. Check that the path is correct.";
                    string caption = "File doesn't exist";
                    MessageBoxButtons button = MessageBoxButtons.OK;
                    MessageBox.Show(message, caption, button);
                }
            }
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {        
            System.Diagnostics.Process.Start("https://gitlab.com/adamunger0/googleearthcreator/wikis/home");
            //System.Diagnostics.Process.Start("https://github.com/xwhiteknuckle22/GoogleEarthFileCreator/wiki");
        }

        private void reportABugToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string address = "adamunger0@gmail.com";
            string subject = "GoogleEarthCreator comment";
            string body = "";
            string attach = "";
            string mailto = string.Format("mailto:{0}?Subject={1}&Body={2}&Attach={3}", address, subject, body, attach);
            System.Diagnostics.Process.Start(mailto);
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveAsFileDialog.FileName = Path.GetFileNameWithoutExtension(csvParser.filePath); // suggest similar output filename as the input filename
            if (csvParser.foundLat && csvParser.foundLong && saveAsFileDialog.ShowDialog() == DialogResult.OK)
            {
                progressPanel.Visible = true;
                fileToolStripMenuItem.Enabled = false;
                helpToolStripMenuItem.Enabled = false;
                csvDataGridView.Enabled = false;
                Application.UseWaitCursor = true;
                exportBackgroundWorker.RunWorkerAsync(saveAsFileDialog.FileName);
            }
            else
            {
                string message = "You haven't loaded a *.csv file of Google Earth points yet. Load and edit, or create, a *.csv file, then you can export it.";
                string caption = "Google Earth data not loaded";
                MessageBox.Show(message, caption);
            }
        }

        private void exportBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Do not access the form's BackgroundWorker reference directly.
            // Instead, use the reference provided by the sender parameter.
            BackgroundWorker bw = sender as BackgroundWorker;

            // Extract the argument.
            string arg = (string)e.Argument;
            csvParser.export(bw, arg, uploadsDataSet.uploadsTable, e);

            // If the operation was canceled by the user, 
            // set the DoWorkEventArgs.Cancel property to true.
            if (bw.CancellationPending)
            {
                e.Cancel = true;
            }
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //Console.WriteLine(e.ProgressPercentage.ToString());
            progressLabel.Text = e.ProgressPercentage.ToString() + "%" + " - " + e.UserState;
            progressLabel.Update();
            progressBar.Value = e.ProgressPercentage;
            progressBar.Update();
        }

        private void exportBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressPanel.Visible = false;
            fileToolStripMenuItem.Enabled = true;
            helpToolStripMenuItem.Enabled = true;
            csvDataGridView.Enabled = true;
            Application.UseWaitCursor = false;
            if (e.Cancelled)
            {
                // The user canceled the operation.
                string message = "The export was cancelled";
                string caption = "Export Cancelled";
                MessageBox.Show(message, caption);
            }
            else if (e.Error != null)
            {
                // There was an error during the operation.
                string msg = String.Format("An error occurred: {0}", e.Error.Message);
                MessageBox.Show(msg);
            }
            else
            {
                // The operation completed normally. 
                string message = String.Format("A Google Earth file was successfully created at: {0}", e.Result);
                string caption = "Export Successful";
                MessageBox.Show(message, caption);
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openCSVFileDialog.ShowDialog() == DialogResult.OK)
            {
                // Create the parser object from the input file
                try
                {
                    progressPanel.Visible = true;
                    fileToolStripMenuItem.Enabled = false;
                    helpToolStripMenuItem.Enabled = false;
                    csvDataGridView.Enabled = false;
                    Application.UseWaitCursor = true;
                    uploadDataSetBindingSource.DataSource = null;
                    parseBackgroundWorker.RunWorkerAsync(openCSVFileDialog.FileName);
                }
                catch (System.IO.IOException)
                {
                    string message = openCSVFileDialog.FileName + " is already open in another program. Make sure that all instances of it are closed and try again.";
                    string caption = "File busy";
                    MessageBoxButtons button = MessageBoxButtons.OK;
                    MessageBox.Show(message, caption, button);
                }
            }
        }

        private void parseBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // Do not access the form's BackgroundWorker reference directly.
            // Instead, use the reference provided by the sender parameter.
            BackgroundWorker bw = sender as BackgroundWorker;

            // Extract the argument.
            string arg = (string)e.Argument;
            csvParser.parse(bw, arg, uploadsDataSet.uploadsTable);

            // If the operation was canceled by the user, 
            // set the DoWorkEventArgs.Cancel property to true.
            if (bw.CancellationPending)
            {
                e.Cancel = true;
            }
        }

        private void parseBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            progressPanel.Visible = false;
            fileToolStripMenuItem.Enabled = true;
            helpToolStripMenuItem.Enabled = true;
            csvDataGridView.Enabled = true;
            Application.UseWaitCursor = false;
            uploadDataSetBindingSource.DataSource = uploadsDataSet.uploadsTable;
            if (e.Cancelled)
            {
                // The user canceled the operation.
                string message = String.Format("The import operation for '{0}' was cancelled", csvParser.filePath);
                string caption = "File open cancelled";
                MessageBox.Show(message, caption);
            }
            else if (e.Error != null)
            {
                // There was an error during the operation.
                string msg = String.Format("An error occurred: {0}", e.Error.Message);
                MessageBox.Show(msg);
            }
            else
            {
                // Catch the exception just in case the bound data is empty and there isn't a 0 index
                try
                {
                    csvDataGridView.Columns[0].Visible = false;
                }
                catch (Exception) { }

                // Update the information labels
                numLongLabel.Text = csvParser.numLong.ToString();
                numLatLabel.Text = csvParser.numLat.ToString();
                numFoldersLabel.Text = csvParser.numFolders.ToString();
                foundLatLabel.Text = csvParser.foundLat ? "Yes" : "No";
                foundLongLabel.Text = csvParser.foundLong ? "Yes" : "No";
                foundNameLabel.Text = csvParser.foundName ? "Yes" : "No";
                foundDescriptionLabel.Text = csvParser.foundDescription ? "Yes" : "No";
                foundIconLabel.Text = csvParser.foundIcon ? "Yes" : "No";

                // The operation completed normally. Display success message to user
                statsGroupBox.Text = csvParser.filePath;
                string message = String.Format("{0} was successfully imported", csvParser.filePath);
                string caption = "Import Successful";
                MessageBox.Show(message, caption);
            }
            progressPanel.Visible = false;
        }
    }
}
