﻿using System;

namespace GoogleEarthCreator
{
    class Point
    {
        Coordinate latitude;
        Coordinate longitude;
        public bool isValid;
        
        public Point(string lat, string lon)
        {
            latitude = new Coordinate(lat);
            longitude = new Coordinate(lon);
            valid();
        }
        public Point(Coordinate lat, Coordinate lon)
        {
            latitude = lat;
            longitude = lon;
            valid();
        }

        private void valid()
        {
            if (latitude != null && latitude.signedness != Coordinate.UNKNOWN && longitude != null && longitude.signedness != Coordinate.UNKNOWN)
                isValid = true;
        }

        public override string ToString()
        {
            return longitude.ToString() + ',' + latitude.ToString();
        }

        public override bool Equals(Object other)
        {
            return other is Point && Equals((Point)other);
        }

        public bool Equals(Point other)
        {
            return latitude == other.latitude && longitude == other.longitude;
        }

        public override int GetHashCode()
        {
            return latitude.GetHashCode() ^ longitude.GetHashCode();
        }
    }
}
