﻿namespace GoogleEarthCreator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportABugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openCSVFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.statsGroupBox = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.numLongLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.foundLongLabel = new System.Windows.Forms.Label();
            this.foundLatLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numFoldersLabel = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.foundIconLabel = new System.Windows.Forms.Label();
            this.foundDescriptionLabel = new System.Windows.Forms.Label();
            this.foundNameLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.numLatLabel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.progressPanel = new System.Windows.Forms.Panel();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.progressLabel = new System.Windows.Forms.Label();
            this.csvDataGridView = new System.Windows.Forms.DataGridView();
            this.uploadDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.saveAsFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.exportBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.parseBackgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.uploadsDataSet = new GoogleEarthCreator.uploadsDataSet();
            this.tableAdapterManager = new GoogleEarthCreator.uploadsDataSetTableAdapters.TableAdapterManager();
            this.uploadsTableAdapter = new GoogleEarthCreator.uploadsDataSetTableAdapters.uploadsTableAdapter();
            this.menuStrip.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.statsGroupBox.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.progressPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.csvDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uploadDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uploadsDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(5, 1, 0, 1);
            this.menuStrip.Size = new System.Drawing.Size(869, 26);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.exportToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.ShortcutKeyDisplayString = "";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(178, 26);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(178, 26);
            this.exportToolStripMenuItem.Text = "Export";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(178, 26);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem1,
            this.reportABugToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.H)));
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(171, 26);
            this.helpToolStripMenuItem1.Text = "Help";
            this.helpToolStripMenuItem1.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // reportABugToolStripMenuItem
            // 
            this.reportABugToolStripMenuItem.Name = "reportABugToolStripMenuItem";
            this.reportABugToolStripMenuItem.Size = new System.Drawing.Size(171, 26);
            this.reportABugToolStripMenuItem.Text = "Report a bug";
            this.reportABugToolStripMenuItem.Click += new System.EventHandler(this.reportABugToolStripMenuItem_Click);
            // 
            // openCSVFileDialog
            // 
            this.openCSVFileDialog.Filter = "CSV files (*.csv)|*.csv";
            this.openCSVFileDialog.Title = "Open CSV";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.statsGroupBox, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 26);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 122F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(869, 570);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // statsGroupBox
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.statsGroupBox, 2);
            this.statsGroupBox.Controls.Add(this.tableLayoutPanel2);
            this.statsGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.statsGroupBox.Location = new System.Drawing.Point(3, 2);
            this.statsGroupBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.statsGroupBox.Name = "statsGroupBox";
            this.statsGroupBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.statsGroupBox.Size = new System.Drawing.Size(863, 118);
            this.statsGroupBox.TabIndex = 6;
            this.statsGroupBox.TabStop = false;
            this.statsGroupBox.Text = "Stats";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.numLongLabel, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.foundLongLabel, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.foundLatLabel, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.numFoldersLabel, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label7, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.label6, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.label5, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.foundIconLabel, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.foundDescriptionLabel, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.foundNameLabel, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.label8, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.numLatLabel, 3, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 17);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(857, 99);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Number of longitude points:";
            // 
            // numLongLabel
            // 
            this.numLongLabel.AutoSize = true;
            this.numLongLabel.Location = new System.Drawing.Point(223, 0);
            this.numLongLabel.Name = "numLongLabel";
            this.numLongLabel.Size = new System.Drawing.Size(16, 17);
            this.numLongLabel.TabIndex = 2;
            this.numLongLabel.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(191, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "Found Longitude column:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(178, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Found Latitude column:";
            // 
            // foundLongLabel
            // 
            this.foundLongLabel.AutoSize = true;
            this.foundLongLabel.Location = new System.Drawing.Point(223, 69);
            this.foundLongLabel.Name = "foundLongLabel";
            this.foundLongLabel.Size = new System.Drawing.Size(26, 17);
            this.foundLongLabel.TabIndex = 11;
            this.foundLongLabel.Text = "No";
            // 
            // foundLatLabel
            // 
            this.foundLatLabel.AutoSize = true;
            this.foundLatLabel.Location = new System.Drawing.Point(223, 46);
            this.foundLatLabel.Name = "foundLatLabel";
            this.foundLatLabel.Size = new System.Drawing.Size(26, 17);
            this.foundLatLabel.TabIndex = 10;
            this.foundLatLabel.Text = "No";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(143, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Number of folders:";
            // 
            // numFoldersLabel
            // 
            this.numFoldersLabel.AutoSize = true;
            this.numFoldersLabel.Location = new System.Drawing.Point(223, 23);
            this.numFoldersLabel.Name = "numFoldersLabel";
            this.numFoldersLabel.Size = new System.Drawing.Size(16, 17);
            this.numFoldersLabel.TabIndex = 16;
            this.numFoldersLabel.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(303, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(149, 17);
            this.label7.TabIndex = 9;
            this.label7.Text = "Found Icon column:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(303, 46);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(201, 17);
            this.label6.TabIndex = 8;
            this.label6.Text = "Found Description column:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(303, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(160, 17);
            this.label5.TabIndex = 7;
            this.label5.Text = "Found Name column:";
            // 
            // foundIconLabel
            // 
            this.foundIconLabel.AutoSize = true;
            this.foundIconLabel.Location = new System.Drawing.Point(523, 69);
            this.foundIconLabel.Name = "foundIconLabel";
            this.foundIconLabel.Size = new System.Drawing.Size(26, 17);
            this.foundIconLabel.TabIndex = 14;
            this.foundIconLabel.Text = "No";
            // 
            // foundDescriptionLabel
            // 
            this.foundDescriptionLabel.AutoSize = true;
            this.foundDescriptionLabel.Location = new System.Drawing.Point(523, 46);
            this.foundDescriptionLabel.Name = "foundDescriptionLabel";
            this.foundDescriptionLabel.Size = new System.Drawing.Size(26, 17);
            this.foundDescriptionLabel.TabIndex = 13;
            this.foundDescriptionLabel.Text = "No";
            // 
            // foundNameLabel
            // 
            this.foundNameLabel.AutoSize = true;
            this.foundNameLabel.Location = new System.Drawing.Point(523, 23);
            this.foundNameLabel.Name = "foundNameLabel";
            this.foundNameLabel.Size = new System.Drawing.Size(26, 17);
            this.foundNameLabel.TabIndex = 12;
            this.foundNameLabel.Text = "No";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(303, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(196, 17);
            this.label8.TabIndex = 17;
            this.label8.Text = "Number of latitude points:";
            // 
            // numLatLabel
            // 
            this.numLatLabel.AutoSize = true;
            this.numLatLabel.Location = new System.Drawing.Point(523, 0);
            this.numLatLabel.Name = "numLatLabel";
            this.numLatLabel.Size = new System.Drawing.Size(16, 17);
            this.numLatLabel.TabIndex = 18;
            this.numLatLabel.Text = "0";
            // 
            // groupBox1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.groupBox1, 2);
            this.groupBox1.Controls.Add(this.progressPanel);
            this.groupBox1.Controls.Add(this.csvDataGridView);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 124);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(863, 444);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Points";
            // 
            // progressPanel
            // 
            this.progressPanel.Controls.Add(this.progressBar);
            this.progressPanel.Controls.Add(this.progressLabel);
            this.progressPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressPanel.Location = new System.Drawing.Point(3, 402);
            this.progressPanel.Name = "progressPanel";
            this.progressPanel.Size = new System.Drawing.Size(857, 40);
            this.progressPanel.TabIndex = 1;
            this.progressPanel.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBar.Location = new System.Drawing.Point(0, 17);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(857, 23);
            this.progressBar.Step = 1;
            this.progressBar.TabIndex = 1;
            // 
            // progressLabel
            // 
            this.progressLabel.AutoSize = true;
            this.progressLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.progressLabel.Location = new System.Drawing.Point(0, 0);
            this.progressLabel.Name = "progressLabel";
            this.progressLabel.Size = new System.Drawing.Size(0, 17);
            this.progressLabel.TabIndex = 0;
            // 
            // csvDataGridView
            // 
            this.csvDataGridView.AllowUserToAddRows = false;
            this.csvDataGridView.AllowUserToDeleteRows = false;
            this.csvDataGridView.AutoGenerateColumns = false;
            this.csvDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.csvDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.csvDataGridView.DataSource = this.uploadDataSetBindingSource;
            this.csvDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.csvDataGridView.Location = new System.Drawing.Point(3, 17);
            this.csvDataGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.csvDataGridView.Name = "csvDataGridView";
            this.csvDataGridView.RowTemplate.Height = 28;
            this.csvDataGridView.Size = new System.Drawing.Size(857, 425);
            this.csvDataGridView.TabIndex = 0;
            // 
            // saveAsFileDialog
            // 
            this.saveAsFileDialog.Filter = "Google Earth (*.kmz)|*.kmz";
            this.saveAsFileDialog.Title = "Save As";
            // 
            // exportBackgroundWorker
            // 
            this.exportBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.exportBackgroundWorker_DoWork);
            this.exportBackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            this.exportBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.exportBackgroundWorker_RunWorkerCompleted);
            // 
            // parseBackgroundWorker
            // 
            this.parseBackgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.parseBackgroundWorker_DoWork);
            this.parseBackgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            this.parseBackgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.parseBackgroundWorker_RunWorkerCompleted);
            // 
            // uploadsDataSet
            // 
            this.uploadsDataSet.DataSetName = "uploadsDataSet";
            this.uploadsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.UpdateOrder = GoogleEarthCreator.uploadsDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            this.tableAdapterManager.uploadsTableAdapter = this.uploadsTableAdapter;
            // 
            // uploadsTableAdapter
            // 
            this.uploadsTableAdapter.ClearBeforeFill = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 596);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Google Earth Creator";
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.statsGroupBox.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.progressPanel.ResumeLayout(false);
            this.progressPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.csvDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uploadDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uploadsDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openCSVFileDialog;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label numLongLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label foundLatLabel;
        private System.Windows.Forms.Label foundLongLabel;
        private System.Windows.Forms.Label foundNameLabel;
        private System.Windows.Forms.Label foundDescriptionLabel;
        private System.Windows.Forms.Label foundIconLabel;
        private System.Windows.Forms.GroupBox statsGroupBox;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView csvDataGridView;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label numFoldersLabel;
        private System.Windows.Forms.SaveFileDialog saveAsFileDialog;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem reportABugToolStripMenuItem;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label numLatLabel;
        private System.Windows.Forms.Panel progressPanel;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label progressLabel;
        private System.ComponentModel.BackgroundWorker exportBackgroundWorker;
        private System.ComponentModel.BackgroundWorker parseBackgroundWorker;
        private System.Windows.Forms.BindingSource uploadDataSetBindingSource;
        private uploadsDataSet uploadsDataSet;
        private uploadsDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private uploadsDataSetTableAdapters.uploadsTableAdapter uploadsTableAdapter;
    }
}

