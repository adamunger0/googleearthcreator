using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.Compression;
using System.Xml;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Microsoft.VisualBasic.FileIO;

namespace GoogleEarthCreator
{
    class ParseCSV
    {
        public const string LAT_HEADER_TITLE = "latitude";
        public const string LONG_HEADER_TITLE = "longitude";
        public const string NAME_HEADER_TITLE = "nameField";
        public const string DESCRIPTION_HEADER_TITLE = "description";
        public const string ICON_HEADER_TITLE = "icon";
        public const string FOLDER_HEADER_TITLE = "folder";

        public bool foundLat;
        public bool foundLong;
        public bool foundName;
        public bool foundDescription;
        public bool foundIcon;
        public bool foundFolder;
        public int numLat;
        public int numLong;
        public int numFolders;
        public string filePath;

        // Parse the file at filePath into lists of columns. The columns that this looks for are: lat, long, name, description, icon
        // The caller is responsible to catch any exceptions that may be thrown from file opening and access.
        public void parse(BackgroundWorker bw, string path, DataTable uploadsTable)
        {
            // Report progress
            if (bw.CancellationPending) return;
            else displayProgress(bw, 0, 1, "Initializing...");

            // We need these variables to determine how long this method will take
            //string firstLine = File.ReadLines(path).First();
            //string[] headers = firstLine.Split(',');
            TextFieldParser parser = new TextFieldParser(path);
            parser.HasFieldsEnclosedInQuotes = true;
            parser.SetDelimiters(",");
            string[] headers = parser.ReadFields(); // returns first line as an array and advances the line pointer...

            // Set the reporting variables
            int currentProgress = 0;
            int totalProgressCount = 3 + (File.ReadLines(path).Count() - 1) + headers.Length;

            // Clear the uploads database table of any possible previous data
            uploadsTable.Clear();

            // Report progress
            if (bw.CancellationPending) return;
            else currentProgress = displayProgress(bw, currentProgress, totalProgressCount, "Creating variables");

            filePath = path;
            var latIndex = int.MinValue;
            var longIndex = int.MinValue;
            var nameIndex = int.MinValue;
            var descriptionIndex = int.MinValue;
            var iconIndex = int.MinValue;
            var folderIndex = int.MinValue;
            // For tracking the number of latitude points, longitude points, and folders
            numLat = 0;
            numLong = 0;
            HashSet<string> numFoldersHash = new HashSet<string>();

            // Iterate through the array of headers and find each header index
            foreach (string header in headers)
            {
                // Report progress
                if (bw.CancellationPending) return;
                else currentProgress = displayProgress(bw, currentProgress, totalProgressCount, String.Format("Parsing {0} header", header));

                var lower = header.ToLower();
                if (lower.Contains("lat"))
                {
                    foundLat = true;
                    latIndex = Array.IndexOf(headers, header);
                }
                else if (lower.Contains("long"))
                {
                    foundLong = true;
                    longIndex = Array.IndexOf(headers, header);
                }
                else if (lower.Contains("name"))
                {
                    foundName = true;
                    nameIndex = Array.IndexOf(headers, header);
                }
                else if (lower.Contains("description"))
                {
                    foundDescription = true;
                    descriptionIndex = Array.IndexOf(headers, header);
                }
                else if (lower.Contains("icon"))
                {
                    foundIcon = true;
                    iconIndex = Array.IndexOf(headers, header);
                }
                else if (lower.Contains("folder"))
                {
                    foundFolder = true;
                    folderIndex = Array.IndexOf(headers, header);
                }
            }

            // Parse each line in the file, but skip the first as it's just the header names
            int i = 0; // this will be the unique index id for each row in the DB
            string[] fields;
            string latitude = String.Empty;
            string longitude = String.Empty;
            string name = String.Empty;
            string description = String.Empty;
            string icon = String.Empty;
            string folder = String.Empty;
            while (!parser.EndOfData)
            {
                // Get this lines' fields
                fields = parser.ReadFields();

                // Report progress
                if (bw.CancellationPending) return;
                else currentProgress = displayProgress(bw, currentProgress, totalProgressCount, String.Format("Parsing line {0} of {1}", i, filePath));

                // Init these to sane defaults
                latitude = String.Empty;
                longitude = String.Empty;
                name = String.Empty;
                description = String.Empty;
                icon = String.Empty;
                folder = String.Empty;

                if (foundLat)
                {
                    latitude = fields[latIndex];
                    numLat += 1;
                }
                if (foundLong)
                {
                    longitude = fields[longIndex];
                    numLong += 1;
                }
                if (foundName)
                    name = fields[nameIndex];
                if (foundDescription)
                    description = fields[descriptionIndex];
                if (foundIcon)
                    icon = fields[iconIndex];
                if (foundFolder)
                {
                    folder = fields[folderIndex];
                    if (folder != String.Empty) numFoldersHash.Add(folder);
                }

                uploadsTable.Rows.Add(i, latitude, longitude, name, description, icon, folder);

                ++i;
            }

            // Get the count of folders
            numFolders = numFoldersHash.Count;

            // Report progress
            displayProgress(bw, 1, 1, "Parsing complete");
        }

        private void removeTempFiles(string tempProjectFolder, string tempFilesFolder)
        {
            // Delete the temporary project folders
            if (Directory.Exists(tempFilesFolder)) // files (no nested directories)
            {
                DirectoryInfo di = new DirectoryInfo(tempFilesFolder);
                foreach (FileInfo f in di.GetFiles()) f.Delete();
                Directory.Delete(tempFilesFolder);
            }
            if (Directory.Exists(tempProjectFolder)) // project folder, may have a nested directory (files)
            {
                DirectoryInfo di = new DirectoryInfo(tempProjectFolder);
                foreach (FileInfo f in di.GetFiles()) f.Delete();
                foreach (DirectoryInfo d in di.GetDirectories()) d.Delete();
                Directory.Delete(tempProjectFolder);
            }
        }

        private int displayProgress(BackgroundWorker bw, int progress, int progressTotal, string message)
        {
            ////Console.WriteLine(progress.ToString() + " " + progressTotal.ToString() + " " + " " + ((double)progress / (double)progressTotal) + " " + ((int)Math.Round((double)(progress / progressTotal * 100))).ToString());
            bw.ReportProgress((int)Math.Round((double)progress / (double)progressTotal * 100.0), message);
            return progress + 1;
         
        }

        private void exportInThread(BackgroundWorker bw, string outputFilePath, DataTable uploadsTable)
        {
            // Report progress and return if a cancellation is pending
            if (bw.CancellationPending) return;
            else displayProgress(bw, 0, 1, "Initiating export...");

            // Create a list of the unique icons
            HashSet<string> uniqueIcons = new HashSet<string>(); // for styles
            foreach (DataRow dr in uploadsTable.Rows) uniqueIcons.Add((string)dr[ICON_HEADER_TITLE]);
            // Create a list of each unique folder
            HashSet<string> uniqueFolders = new HashSet<string>();
            foreach (DataRow dr in uploadsTable.Rows) uniqueFolders.Add((string)dr[FOLDER_HEADER_TITLE]);

            int totalProgressCount = 6 + (uploadsTable.Rows.Count * 2) + uniqueIcons.Count + uniqueFolders.Count;
            int currentProgress = 0;            

            // Variables to define file storage locations
            string tempProjectFolder = Path.GetTempPath() + "GoogleEarthCreatorTemp"; // to contain the entire unzipped Google Earth project
            string tempFilesFolder = tempProjectFolder + Path.DirectorySeparatorChar + "files"; // to contain any additional resources that may be necessary
            string tempKmlFilePath = tempProjectFolder + Path.DirectorySeparatorChar + "doc.kml"; // file to contain the kml code

            // Cleanup the temp files. This may seem kind of redundant as these files are already cleaned up at the end of this thread, however, if there's ever an error where the application shutsdown before the cleaning can happen, then this will prevent program crashes from exceptions.
            // We will leave the cleanup at the end as well so that we aren't storing useless files and wasting disk space.
            removeTempFiles(tempProjectFolder, tempFilesFolder);

            // Report progress
            if (bw.CancellationPending) return;
            else currentProgress = displayProgress(bw, currentProgress, totalProgressCount, "Setting up XML boilerplate");

            // Create the directory that we will be working in
            Directory.CreateDirectory(tempProjectFolder);

            // Start by creating the kml file
            XmlWriterSettings settings = new XmlWriterSettings { Indent = true };
            XmlWriter xmlWriter = XmlWriter.Create(tempKmlFilePath, settings);

            // The boilerplate xml stuff
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("kml", "http://www.opengis.net/kml/2.2");
            xmlWriter.WriteAttributeString("xmlns", "gx", null, "http://www.google.com/kml/ext/2.2");
            xmlWriter.WriteAttributeString("xmlns", "kml", null, "http://www.opengis.net/kml/2.2");
            xmlWriter.WriteAttributeString("xmlns", "atom", null, "http://www.w3.org/2005/Atom");

            // Create the root elements of the kml file
            xmlWriter.WriteStartElement("Document");
            xmlWriter.WriteStartElement("name");
            xmlWriter.WriteString(Path.GetFileName(outputFilePath)); // filename of kmz project NOT the kml file inside that project
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("open");
            xmlWriter.WriteString("0");
            xmlWriter.WriteEndElement();

            // Report progress
            if (bw.CancellationPending) return;
            else currentProgress = displayProgress(bw, currentProgress, totalProgressCount, "Creating style elements");

            // Create style elements for each different icon
            Dictionary<string, string> styleIDs = new Dictionary<string, string>();
            foreach (string icon in uniqueIcons)
            {
                // Report progress
                if (bw.CancellationPending) return;
                else currentProgress = displayProgress(bw, currentProgress, totalProgressCount, String.Format("Creating style element for {0}", icon));

                if (!icon.Equals(String.Empty))
                {
                    string id = "styleID" + styleIDs.Count;
                    styleIDs.Add(icon, id);
                    xmlWriter.WriteStartElement("Style");
                    xmlWriter.WriteAttributeString("id", id);
                    xmlWriter.WriteStartElement("IconStyle");
                    xmlWriter.WriteStartElement("Icon");
                    xmlWriter.WriteStartElement("href");
                    xmlWriter.WriteString(icon);
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteEndElement();
                }
            }

            // Create folders with points inside them
            // 1 iterate through each unique folder name
            // 2 iterate through each index of the lists (i.e. for (int i = 0; i < numpoints; ++i))
            // 3 if an index within the folders list matches the current unique folder name that we
            // are looking at, add that point to the kml file using the matching style id from above
            // 4 add the points that aren't associated to a folder
            foreach (string uniqueFolderName in uniqueFolders) // iterate through each unique folder name
            {
                // Report progress
                if (bw.CancellationPending) return;
                else currentProgress = displayProgress(bw, currentProgress, totalProgressCount, String.Format("Creating {0} folder", uniqueFolderName));

                if (!uniqueFolderName.Equals(String.Empty))
                {
                    // Add this folder to the kml file
                    xmlWriter.WriteStartElement("Folder");
                    xmlWriter.WriteStartElement("name");
                    xmlWriter.WriteString(uniqueFolderName);
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteStartElement("open");
                    xmlWriter.WriteString("0");
                    xmlWriter.WriteEndElement();
                    foreach (DataRow row in uploadsTable.Rows)
                    {
                        if (((string)row[FOLDER_HEADER_TITLE]).Equals(uniqueFolderName))
                        {
                            // Add this point to the kml file under this folder
                            xmlWriter.WriteStartElement("Placemark");
                            xmlWriter.WriteStartElement("name");
                            xmlWriter.WriteString((string)row[NAME_HEADER_TITLE]);
                            xmlWriter.WriteEndElement();
                            xmlWriter.WriteStartElement("description");
                            xmlWriter.WriteString((string)row[DESCRIPTION_HEADER_TITLE]);
                            xmlWriter.WriteEndElement();
                            xmlWriter.WriteStartElement("styleUrl");
                            string icon = String.Empty;
                            styleIDs.TryGetValue((string)row[ICON_HEADER_TITLE], out icon);
                            xmlWriter.WriteString(icon);
                            xmlWriter.WriteEndElement();
                            xmlWriter.WriteStartElement("Point");
                            xmlWriter.WriteStartElement("coordinates");
                            Point p = new Point((string)row[LAT_HEADER_TITLE], (string)row[LONG_HEADER_TITLE]);
                            xmlWriter.WriteString(p.ToString());
                            xmlWriter.WriteEndElement(); // close the coordinates element
                            xmlWriter.WriteEndElement(); // close the Point element
                            xmlWriter.WriteEndElement(); // close the Placemark element
                        }
                    }
                    xmlWriter.WriteEndElement(); // close the folder element
                }
            }

            // Create all points that aren't associated with a folder
            foreach (DataRow row in uploadsTable.Rows) // iterate through all of the points and write the ones that aren't associated with a folder to the kml file
            {
                // Report progress
                if (bw.CancellationPending) return;
                else currentProgress = displayProgress(bw, currentProgress, totalProgressCount, "Creating points without a folder");

                if (((string)row[FOLDER_HEADER_TITLE]).Equals(String.Empty))
                {
                    // Add this index to the kml file
                    xmlWriter.WriteStartElement("Placemark");
                    xmlWriter.WriteStartElement("name");
                    xmlWriter.WriteString((string)row[NAME_HEADER_TITLE]);
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteStartElement("description");
                    xmlWriter.WriteString((string)row[DESCRIPTION_HEADER_TITLE]);
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteStartElement("styleUrl");
                    string icon = String.Empty;
                    styleIDs.TryGetValue((string)row[ICON_HEADER_TITLE], out icon);
                    xmlWriter.WriteString(icon);
                    xmlWriter.WriteEndElement();
                    xmlWriter.WriteStartElement("Point");
                    xmlWriter.WriteStartElement("coordinates");
                    Point p = new Point((string)row[LAT_HEADER_TITLE], (string)row[LONG_HEADER_TITLE]);
                    xmlWriter.WriteString(p.ToString());
                    xmlWriter.WriteEndElement(); // close the coordinates element
                    xmlWriter.WriteEndElement(); // close the Point element
                    xmlWriter.WriteEndElement(); // close the Placemark element
                }
            }
            xmlWriter.WriteEndDocument();
            xmlWriter.Close();

            // Now copy any icons that were referenced locally to a folder
            foreach (DataRow row in uploadsTable.Rows)
            {
                // Report progress
                if (bw.CancellationPending) return;
                else currentProgress = displayProgress(bw, currentProgress, totalProgressCount, String.Format("Evaluating {0}", (string)row[ICON_HEADER_TITLE]));

                try
                {
                    if (!((string)row[ICON_HEADER_TITLE]).ToLower().Contains("http") && File.Exists((string)row[ICON_HEADER_TITLE])) // This will throw System.ArgumentException if the uri isn't well formed. It can be ignored.
                    {
                        if (!Directory.Exists(tempFilesFolder)) Directory.CreateDirectory(tempFilesFolder);
                        string fn = tempFilesFolder + Path.DirectorySeparatorChar + ((string)row[ICON_HEADER_TITLE]).Split(Path.DirectorySeparatorChar).Last();
                        if (!File.Exists(fn)) File.Copy((string)row[ICON_HEADER_TITLE], fn);
                    }
                }
                catch (System.ArgumentException)
                {
                    // not much that we can do here since if the file doesn't exist or wasn't able to be copied, then the user has screwed some stuff up that we can't fix. They will notice when the file is loaded into Google Earth.....
                }
            }

            // Report progress
            if (bw.CancellationPending) return;
            else currentProgress = displayProgress(bw, currentProgress, totalProgressCount, "Creating KMZ file");

            // Zip the resources folder and the doc.kml file together into a *.kmz file.
            // If a file of the same name exists, remove it (the user selected this option on the file save dialog).
            if (File.Exists(outputFilePath)) File.Delete(outputFilePath);
            ZipFile.CreateFromDirectory(tempProjectFolder, outputFilePath);

            // Report progress
            if (bw.CancellationPending) return;
            else currentProgress = displayProgress(bw, currentProgress, totalProgressCount, "Cleaning up temporary files");

            // Cleanup the temp files
            removeTempFiles(tempProjectFolder, tempFilesFolder);

            // Report progress
            displayProgress(bw, 1, 1, "Export successful");
        }

        private int validateLatLong(DataTable uploadsTable)
        {
            int errorCount = 0; // just in case the DataTable is null
            foreach (DataRow row in uploadsTable.Rows)
            {
                try
                {
                    Coordinate c = new Coordinate((string)row[LAT_HEADER_TITLE]);
                    if (c.format == Coordinate.UNKNOWN || c.signedness == Coordinate.UNKNOWN)
                    {
                        //Console.WriteLine("lat...errorCount: " + errorCount.ToString() + ", c.signedDD: " + c.signedDD.ToString() + ", c.format: " + c.format.ToString() + ", c.signedness: " + c.signedness.ToString());
                        ++errorCount;
                    }

                    Coordinate c1 = new Coordinate((string)row[LONG_HEADER_TITLE]);
                    if (c1.format == Coordinate.UNKNOWN || c1.signedness == Coordinate.UNKNOWN)
                    {
                        //Console.WriteLine("long...errorCount: " + errorCount.ToString() + ", c1.signedDD: " + c1.signedDD.ToString() + ", c1.format: " + c1.format.ToString() + ", c1.signedness: " + c1.signedness.ToString());
                        ++errorCount;
                    }
                }
                catch (System.InvalidCastException e)
                {
                    //Console.WriteLine("Error validating input: " + e.ToString());
                    ++errorCount;
                }
            }
            return errorCount;
        }

        // Export the parsed csv data into a kmz file. For this to work, parse has to have been called first and successfully completed.
        public void export(BackgroundWorker bw, string fp, DataTable uploadsTable, DoWorkEventArgs e)
        {
            int errors = validateLatLong(uploadsTable);
            if (errors != 0)
            {
                string errorPlurality = errors == 1 ? "error" : "errors";
                string isPlurality = errors == 1 ? "is" : "are";
                string themPlurality = errors == 1 ? "it" : "them";
                string message = String.Format("There {0} {1} {2} with the coordinates. Fix {3} and try again.", isPlurality, errors.ToString(), errorPlurality, themPlurality);
                string caption = "Errors";
                MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
                return;
            }
            else
            {
                exportInThread(bw, fp, uploadsTable);
            }

            e.Result = fp;
        }
    }
}
