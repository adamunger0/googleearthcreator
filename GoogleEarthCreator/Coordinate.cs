using System;
using System.Collections.Generic;
using System.Linq;

namespace GoogleEarthCreator
{
    class Coordinate
    {
        // Constants for coordinate conversion
        public const int UNKNOWN = 0;
        public const int LATITUDE = 1;
        public const int LONGITUDE = 2;
        public const int SIGNED = 3;
        public const int UNSIGNED = 4;
        public const int DD = 5;
        public const int DDMM = 6;
        public const int DDMMSS = 7;

        public int signedness;
        public int format;
        public string coordinate;
        public double signedDD  = float.NaN;

        // Use this page as a guide: http://www.geomidpoint.com/latlon.html
        public Coordinate(string coord)
        {
            coordinate = coord;
            validateInput();
        }

        public override string ToString()
        {
            return signedDD.ToString();
        }

        public override bool Equals(Object other)
        {
            return other is Coordinate && Equals((Coordinate)other);
        }

        public bool Equals(Coordinate other)
        {
            return signedDD == other.signedDD;
        }

        public override int GetHashCode()
        {
            return signedDD.GetHashCode();
        }

        // Ensure that:
        //      1) input doesn't have negative signs AND letters
        //      2) if signed, ensure that there aren't also letters (s and w get negative sign)
        //      3) if unsigned, ensure that there is only one letter (n, s, e, or w)
        //      4) ensure there are either 1, 2, or 3 groups of numbers
        //      5) ensure that each group of numbers is a valid float
        //      6) ensure that only the last group has decimal digits, if any
        //      7) -90 <= signed lat (n, s) <= 90
        //      8) -180 <= signed long (e, w) <= 180
        //      9) 0 <= unsigned lat (n, s) <= 90
        //      10) 0 <= unsigned long (e, w) <= 180
        private bool validateInput()
        {
            try
            {
                // Determine the type of coordinate and validate
                if (coordinate.IndexOfAny("nsewNSEW".ToArray()) != -1 && validateUnsigned())
                {
                    return true;
                }
                else if (validateSigned())
                {
                    return true;
                }
            }
            catch (System.InvalidOperationException e)
            {
                //Console.WriteLine("Error parsing input coordinate: '" + coordinate + "' " + e.ToString());
            }

            // Set the variables to default values if the input was not able to be parsed. NOTE: this is necessary since in some specific cases some variables could be initialized even if validation fails later.
            signedDD = float.NaN;
            signedness = UNKNOWN;
            format = UNKNOWN;

            return false;
        }

        private bool validateUnsigned()
        {
            signedness = UNSIGNED;
            //Console.WriteLine("unsigned, coordinate: " + coordinate.ToString());
            string tempCoordinate = String.Empty;
            int directionIndex = coordinate.IndexOfAny("nsewNSEW".ToArray());
            char direction = coordinate[directionIndex];
            // Replace the indicator with a space.
            // If the direction indicator is after the degrees value, insert a space after the minutes so the following will think this is a 3 value format.
            if (directionIndex == 2 || directionIndex == 3)
            {
                // Replace direction indicator
                tempCoordinate = coordinate.Replace(direction, ' ');
                // Insert space after MM component
                tempCoordinate = tempCoordinate.Insert(directionIndex + 3, " ");
            }
            else
            {
                // Remove direction indicator
                tempCoordinate = coordinate.Remove(directionIndex);
            }

            ////Console.WriteLine("direction: " + direction + ", tempCoordinate: " + tempCoordinate);
            // Split by space to extract the numbers - there should only be 1, 2, or 3 numbers
            List<string> coordComponents = new List<string>(tempCoordinate.Split(' '));
            if (coordComponents.Count == 1)
            {
                //Console.WriteLine("coordComponents[0] == " + coordComponents[0]);
                format = DD;
                try
                {
                    signedDD = double.Parse(coordComponents[0]);
                    if ("sSwW".ToArray().Contains(direction)) signedDD *= -1.0;
                    if (("sSnN".ToArray().Contains(direction) && signedDD >= -90.0 && signedDD <= 90.0) ||
                        ("eEwW".ToArray().Contains(direction) && signedDD >= -180.0 && signedDD <= 180.0))
                    {
                        //Console.WriteLine("input good");
                        return true;
                    }
                    else
                    {
                        //Console.WriteLine("input bad");
                    }
                }
                catch (Exception e)
                {
                    //Console.WriteLine("Unable to parse double: '" + coordinate + "' " + e.ToString());
                }
            }
            else if (coordComponents.Count == 2)
            {
                //Console.WriteLine("coordComponents[0] == " + coordComponents[0] + ", coordComponents[1] == " + coordComponents[1]);
                format = DDMM;
                // Ensure that there aren't decimals in the DD component
                List<string> s = new List<string>(coordComponents[0].Split('.'));
                if (s.Count == 1)
                {
                    // Now attempt to convert to signed DD.DDDD
                    try
                    {
                        signedDD = double.Parse(coordComponents[0]) + (double.Parse(coordComponents[1]) / 60.0);
                        if ("sSwW".ToArray().Contains(direction)) signedDD *= -1.0;
                        if (("sSnN".ToArray().Contains(direction) && signedDD >= -90.0 && signedDD <= 90.0) ||
                            ("eEwW".ToArray().Contains(direction) && signedDD >= -180.0 && signedDD <= 180.0))
                        {
                            //Console.WriteLine("input good");
                            return true;
                        }
                        else
                        {
                            //Console.WriteLine("input bad");
                        }
                    }
                    catch (Exception e)
                    {
                        //Console.WriteLine("Unable to parse double: '" + coordinate + "' " + e.ToString());
                    }
                }
            }
            else if (coordComponents.Count == 3)
            {
                //Console.WriteLine("coordComponents[0] == " + coordComponents[0] + ", coordComponents[1] == " + coordComponents[1] + ", coordComponents[2] == " + coordComponents[2]);
                format = DDMMSS;
                // Ensure that there aren't decimals in the DD or MM components
                List<string> s1 = new List<string>(coordComponents[0].Split('.'));
                List<string> s2 = new List<string>(coordComponents[1].Split('.'));
                if (s1.Count == 1 && s2.Count == 1)
                {
                    // Now attempt to convert to signed DD.DDDD
                    try
                    {
                        signedDD = double.Parse(coordComponents[0]) + (double.Parse(coordComponents[1]) / 60.0) + (double.Parse(coordComponents[2]) / 3600.0);
                        if ("sSwW".ToArray().Contains(direction)) signedDD *= -1.0;
                        if (("sSnN".ToArray().Contains(direction) && signedDD >= -90.0 && signedDD <= 90.0) ||
                            ("eEwW".ToArray().Contains(direction) && signedDD >= -180.0 && signedDD <= 180.0))
                        {
                            //Console.WriteLine("input good");
                            return true;
                        }
                        else
                        {
                            //Console.WriteLine("input bad");
                        }
                    }
                    catch (Exception e)
                    {
                        //Console.WriteLine("Unable to parse double: '" + coordinate + "' " + e.ToString());
                    }
                }
            }

            return false;
        }

        private bool validateSigned()
        {
            signedness = SIGNED;
            //Console.WriteLine("signed, coordinate: " + coordinate.ToString());
            // If a sign exists, it should be the first character in the string - remove it so the conversions will work as planned
            string noSign = coordinate.Replace("-", "");
            noSign = noSign.Replace("+", "");
            // Split by space to extract the numbers - there should only be 1, 2, or 3 numbers
            List<string> coordComponents = new List<string>(noSign.Split(' '));
            if (coordComponents.Count == 1)
            {
                //Console.WriteLine("coordComponents[0] == " + coordComponents[0]);
                format = DD;
                try
                {
                    signedDD = double.Parse(coordComponents[0]);
                    if (noSign.Count() != coordinate.Count()) signedDD *= -1.0;
                    // If the input is signed, we don't have enough information to distinguish between lat and long.
                    if (signedDD >= -180.0 && signedDD <= 180.0)
                    {
                        //Console.WriteLine("input good");
                        return true;
                    }
                    else
                    {
                        //Console.WriteLine("input bad");
                    }
                }
                catch (Exception e)
                {
                    //Console.WriteLine("Unable to parse double: '" + coordinate + "' " + e.ToString());
                }
            }
            else if (coordComponents.Count == 2)
            {
                //Console.WriteLine("coordComponents[0] == " + coordComponents[0] + ", coordComponents[1] == " + coordComponents[1]);
                format = DDMM;
                // Ensure that there aren't decimals in the DD component
                List<string> s = new List<string>(coordComponents[0].Split('.'));
                if (s.Count == 1)
                {
                    // Now attempt to convert to signed DD.DDDD
                    try
                    {
                        signedDD = double.Parse(coordComponents[0]) + (double.Parse(coordComponents[1]) / 60.0);
                        if (noSign.Count() != coordinate.Count()) signedDD *= -1.0;
                        // If the input is signed, we don't have enough information to distinguish between lat and long.
                        if (signedDD >= -180.0 && signedDD <= 180.0)
                        {
                            //Console.WriteLine("input good");
                            return true;
                        }
                        else
                        {
                            //Console.WriteLine("input bad");
                        }
                    }
                    catch (Exception e)
                    {
                        //Console.WriteLine("Unable to parse double: '" + coordinate + "' " + e.ToString());
                    }
                }
            }
            else if (coordComponents.Count == 3)
            {
                //Console.WriteLine("coordComponents[0] == " + coordComponents[0] + ", coordComponents[1] == " + coordComponents[1] + ", coordComponents[2] == " + coordComponents[2]);
                format = DDMMSS;
                // Ensure that there aren't decimals in the DD or MM components
                List<string> s1 = new List<string>(coordComponents[0].Split('.'));
                List<string> s2 = new List<string>(coordComponents[1].Split('.'));
                if (s1.Count == 1 && s2.Count == 1)
                {
                    // Now attempt to convert to signed DD.DDDD
                    try
                    {
                        signedDD = double.Parse(coordComponents[0]) + (double.Parse(coordComponents[1]) / 60.0) + (double.Parse(coordComponents[2]) / 3600.0);
                        if (noSign.Count() != coordinate.Count()) signedDD *= -1.0;
                        // If the input is signed, we don't have enough information to distinguish between lat and long.
                        if (signedDD >= -180.0 && signedDD <= 180.0)
                        {
                            //Console.WriteLine("input good");
                            return true;
                        }
                        else
                        {
                            //Console.WriteLine("input bad");
                        }
                    }
                    catch (Exception e)
                    {
                        //Console.WriteLine("Unable to parse double: '" + coordinate + "' " + e.ToString());
                    }
                }
            }
            return false;
        }
    }    
}
